﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Newtonsoft.Json;
using shopping_admin_uwp.Annotations;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace shopping_admin_uwp.Pages.Brands
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BrandsForm : Page, INotifyPropertyChanged
    {
        private BitmapImage _logo = null;

        public BitmapImage Logo
        {
            get => _logo;
            set
            {
                if (value != _logo)
                {
                    _logo = value;
                    OnPropertyChanged();
                }
            }
        }

        public BrandsForm()
        {
            this.InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Open a text file.
            Windows.Storage.Pickers.FileOpenPicker open =
                new Windows.Storage.Pickers.FileOpenPicker();
            open.SuggestedStartLocation =
                Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            open.FileTypeFilter.Add(".jpg");

            Windows.Storage.StorageFile file = await open.PickSingleFileAsync();

            if (file != null)
            {
                try
                {
                    /// <detail>
                    ///     For more infomation, read here: https://cloudinary.com/documentation/dotnet_integration#_net_getting_started_guide
                    /// </detail>
                    Account acc = new Account("fpt-aptech", "686527673371856", "vaPoRw5o6GWHIzILIOrgvligl9c");
                    Cloudinary cloudinary = new Cloudinary(acc);
                    Windows.Storage.Streams.IRandomAccessStream randAccStream =
                        await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    ImageUploadParams imageUploadParams = new ImageUploadParams
                    {
                        File = new FileDescription(DateTime.Now.ToString(), await file.OpenStreamForReadAsync())
                    };

                    Logo = new BitmapImage(cloudinary.Upload(imageUploadParams).SecureUri);
                    LogoPreview.Visibility = Visibility.Visible;
//                    // Load the file into the Document property of the RichEditBox.
//                    editor.Document.LoadFromStream(Windows.UI.Text.TextSetOptions.FormatRtf, randAccStream);
                }
                catch (Exception exp)
                {
                    UWPConsole.BackgroundConsole.WriteLine(exp.Message);
                    ContentDialog errorDialog = new ContentDialog
                    {
                        Title = "File open error",
                        Content = "Sorry, I couldn't open the file.",
                        PrimaryButtonText = "Ok"
                    };

                    await errorDialog.ShowAsync();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void UIElement_OnTapped(object sender, TappedRoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnResetForm(object sender, TappedRoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
