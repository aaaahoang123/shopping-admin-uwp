﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using shopping_admin_uwp.Annotations;
using shopping_admin_uwp.Pages;
using shopping_admin_uwp.Pages.Brands;
using shopping_admin_uwp.Pages.Categories;
using shopping_admin_uwp.Pages.Products;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace shopping_admin_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private Dictionary<string, Type> _pages = new Dictionary<string, Type>
        {
            {"dashboard", typeof(Dashboard)},
            { "brands_form", typeof(BrandsForm) },
            { "brands_table", typeof(BrandsTable) },
            { "categories_form", typeof(CategoriesForm) },
            { "categories_table", typeof(CategoriesTable) },
            { "products_form", typeof(ProductsForm) },
            { "products_table", typeof(ProductsTable) },
            { "setting", typeof(SettingPage) }
        };

        public Dictionary<string, Type> Pages
        {
            get => _pages;
            set {if (_pages != value){_pages = value; OnPropertyChanged();}}
        }

        public MainPage()
        {
            this.InitializeComponent();
            ContentFrame.Navigate(typeof(Dashboard));
        }

        public void HandleNavigate(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            if (args.IsSettingsInvoked)
            {
                ContentFrame.Navigate(Pages["setting"]);
            }
            else
            {
                var itemTag = NavView.MenuItems.OfType<NavigationViewItem>()
                                .First(i => args.InvokedItem.Equals(i.Content))
                                .Tag.ToString();
                ContentFrame.Navigate(Pages[itemTag]);
            }
        
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
